package com.walmart.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.walmart.dto.EdgeDTO;
import com.walmart.dto.StatMsgDTO;
import com.walmart.service.EdgeService;

@Path("/edge")
@Produces(MediaType.APPLICATION_JSON)
public class EdgeController {
	
	@EJB
	private EdgeService edgeService;
	
	@GET
	@Path("")
	public Response listEdges() throws Exception {
		EdgeDTO edgeDTO = edgeService.listAllEdges();
		return Response.status(EdgeDTO.getStatus()).entity(edgeDTO).build();
	}
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createEdge(String data) throws Exception {
		StatMsgDTO sDTO = edgeService.createEdge(data);
		return Response.status(StatMsgDTO.getStatus()).entity(sDTO).build();
	}
	
	@DELETE
	@Path("/remove")
	public Response removeEdge(@QueryParam("id") String id) throws Exception {
		StatMsgDTO sDTO = edgeService.removeEdge(id);
		return Response.status(StatMsgDTO.getStatus()).entity(sDTO).build();
	}
	
		
}
