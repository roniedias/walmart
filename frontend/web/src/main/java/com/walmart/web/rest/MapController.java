package com.walmart.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.walmart.dto.MapDTO;
import com.walmart.service.MapService;


@Path("/map")
@Produces(MediaType.APPLICATION_JSON)
public class MapController {
	
	@EJB
	MapService mapService;
	
	
	@GET
	@Path("/route")
	public Response getRoute(@QueryParam("origin") String origin, @QueryParam("destination") String destination, @QueryParam("autonomy") String autonomy, @QueryParam("fuelprice") String fuelprice) throws Exception {	
		MapDTO mapDTO = mapService.route(origin, destination, autonomy, fuelprice);
		return Response.status(MapDTO.getStatus()).entity(mapDTO).build();
	}
		
		
}
