package com.walmart.web.rest;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.walmart.dto.StatMsgDTO;
import com.walmart.dto.VertexDTO;
import com.walmart.service.VertexService;

@Path("/vertex")
@Produces(MediaType.APPLICATION_JSON)
public class VertexController {
	
	@EJB
	private VertexService vertexService;

	@GET
	@Path("")
	public Response listVertices() throws Exception {
		VertexDTO vertexDTO = vertexService.listAllVertices();
		return Response.status(VertexDTO.getStatus()).entity(vertexDTO).build();
	}
	
	@POST
	@Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createVertex(String data) throws Exception {
		StatMsgDTO sDTO = vertexService.createVertex(data);
		return Response.status(StatMsgDTO.getStatus()).entity(sDTO).build();
	}
	
	@DELETE
	@Path("/remove")
	public Response removeVertex(@QueryParam("id") String id) throws Exception {
		StatMsgDTO sDTO = vertexService.removeVertex(id);
		return Response.status(StatMsgDTO.getStatus()).entity(sDTO).build();
	}


	



}
