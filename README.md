# Teste - Walmart #



**Objetivo:** Apresentar uma possível solução para o desafio abaixo proposto:

 
>*"O Walmart esta desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento. Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo), nesta mesma requisição o requisitante deverá informar um nome para este mapa. É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam. O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.*

>**
A B 10
B D 15
A C 20
C D 30
B E 50
D E 30
**

>*Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho, para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustivel, agora sua tarefa é criar este Webservices. Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,25.*

>*Voce está livre para definir a melhor arquitetura e tecnologias para solucionar este desafio, mas não se esqueça de contar sua motivação no arquivo README que deve acompanhar sua solução, junto com os detalhes de como executar seu programa. Documentação e testes serão avaliados também =) Lembre-se de que iremos executar seu código com malhas beeemm mais complexas, por isso é importante pensar em requisitos não funcionais também!!"*
 
Para este fim, foi desenvolvida uma aplicação que pode ser implantada em qualquer servidor JEE. Para facilitar o ciclo de testes, os serviços foram disponibilizados no ip público 54.232.207.74. As urls relativas a cada serviço serão informadas abaixo.

**Obs:** Como não foi desenvolvida uma interface para execução dos exemplos, pode ser utilizada qualquer ferramenta client para chamadas HTTP. Para os exemplos exibidos nas imagens abaixo, foi utilizado o plugin para Google Chrome *Advanced REST Client*.

**Obs 2:** Os serviços do tipo POST requerem que os dados sejam submetidos no *corpo da requisição* e sejam do tipo *application/json*

Na camada de persistência, foram criadas duas entidades: Vertex (vértice) e Edge (aresta). Vertex representa os pontos ou locais no grafo. Edge representa as distâncias entre cada vértice. Para a realização dos testes, algumas premissas se fazem necessárias:

1. Os vértices devem **necessariamente** ser persistidos na base de dados **antes** das arestas. O serviço do tipo POST para gravação dos vértices está disponível em http://54.232.207.74/walmart/api/rest/vertex/create, e requer um JSON com a chave *name* no corpo da requisição, conforme imagem:


![vertex_persistence.png](https://bitbucket.org/repo/n5xM8g/images/3141321187-vertex_persistence.png)


Grave tantos vértices quanto achar necessário. 

![db_vertices.png](https://bitbucket.org/repo/n5xM8g/images/2182693641-db_vertices.png)

Ao finalizar esta etapa, efetue a gravação das arestas. Igualmente ao serviço de gravação dos vértices, foi disponibilizado um seviço do tipo POST para este fim, no endereço: http://54.232.207.74/walmart/api/rest/edge/create

Este serviço também requer um JSON no corpo da requisição. Um exemplo válido seria:

{
   "origin": "A",
   "destination": "B",
   "distance": 10
}	


![edge_persistence.png](https://bitbucket.org/repo/n5xM8g/images/3926979031-edge_persistence.png)

Mais uma vez, você pode para gravar tantas quantas arestas desejar:

![edge_db_img.png](https://bitbucket.org/repo/n5xM8g/images/3934502368-edge_db_img.png)


**Obs 3:** Tanto **origin** quanto **destination** devem ser vértices válidos (previamente persistidos na tabela vertex). Caso contrário um status **400 Bad Request**, juntamente com a mensagem *Invalid DESTINATION* serão retornados

*
EX: 
{
message: "Invalid DESTINATION: k"
}
*

Por fim, um serviço do tipo GET foi disponibilizado para retornar o cálculo proposto no teste. Este serviço requer que os dados de entrada sejam disponbilizadas via *query param*, conforme o exemplo http://54.232.207.74/walmart/api/rest/map/route?origin=A&destination=D&autonomy=10&fuelprice=2.5 abaixo:


![get_example.png](https://bitbucket.org/repo/n5xM8g/images/1793836129-get_example.png)


# Métodos úteis #

**GET** 
http://54.232.207.74/walmart/api/rest/vertex (Lista todos os vértices)
http://54.232.207.74/walmart/api/rest/edge (Lista todos as arestas)

**DELETE**
http://localhost/walmart/api/rest/vertex/remove?id=3 (Remove o vértice cujo id = 3)

http://localhost/walmart/api/rest/edge/remove?id=8 (Remove a aresta cujo id = 8)

**Obs 4:** Antes de tentar remover um determinado registro de vertex, certifique-se de remover todos os registros da tabela edge que contém campos origin/destination iguais ao vértice que está tentando remover. Caso contrário, será retornada a seguinte mensagem: "*One or more Edges bound to this vertex. You must delete all dependent registers, before executing this operation*"


>Bom teste e obrigado!!!


# Minhas informações #

**Nome:** Ronie Dias Pinto

**Email:** roniedias@yahoo.com

**Telefone:** +55 11 98674-4103