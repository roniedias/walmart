package com.walmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "vertex")
@org.hibernate.annotations.Table(appliesTo = "vertex", indexes = { @Index(name = "VertexI001", columnNames = { "name" }) })
@NamedQueries({
		@NamedQuery(name = Vertex.QUERY_LIST_ALL_VERTICES, query = "SELECT v FROM Vertex v"),
		@NamedQuery(name = Vertex.QUERY_LIST_VERTEX_BY_ID, query = "SELECT v FROM Vertex v WHERE id = :id"),
		@NamedQuery(name = Vertex.QUERY_LIST_VERTEX_BY_NAME, query = "SELECT v FROM Vertex v WHERE name = :name") })
public class Vertex implements Serializable{

	private static final long serialVersionUID = -7369842381112642550L;

	public static final String QUERY_LIST_ALL_VERTICES = "QUERY_LIST_ALL_VERTICES";
	public static final String QUERY_LIST_VERTEX_BY_ID = "QUERY_LIST_VERTEX_BY_ID";
	public static final String QUERY_LIST_VERTEX_BY_NAME = "QUERY_LIST_VERTEX_BY_NAME";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String name;

	
	public long getId() {
		
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


	public Vertex() {}
		
		
	public Vertex(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Vertex [id=" + id + ", name=" + name + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
	    int result = 1;
	    result = prime * result + ((id == null) ? 0 : id.hashCode());
	    return result;
	}
	
	  
	  @Override
	  public boolean equals(Object obj) {
		  if (this == obj) {
			  return true;
		  }  
		  if (obj == null) {
			  return false;
		  }
		  if (getClass() != obj.getClass()) {
			  return false;  
		  }
	      Vertex other = (Vertex) obj;
	      if (id == null) {
	    	  if (other.id != null) {
	    		  return false;
	    	  }
	      } 
	      else if (!id.equals(other.id)) {
	    	  return false;
	      }    
	    return true;
	  }	
	

}