package com.walmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "edge")
@NamedQueries({
	@NamedQuery(name = Edge.QUERY_LIST_EDGE_BY_ID, query = "SELECT e FROM Edge e WHERE id = :id"),
	@NamedQuery(name = Edge.QUERY_LIST_ALL_EDGES, query = "SELECT e FROM Edge e")
})
public class Edge implements Serializable {
	
	private static final long serialVersionUID = -326458490249880297L;
	
	public static final String QUERY_LIST_EDGE_BY_ID = "QUERY_LIST_EDGE_BY_ID";
	public static final String QUERY_LIST_ALL_EDGES = "QUERY_LIST_ALL_EDGES";
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;	
	
	@OneToOne
	private Vertex origin;
	
	@OneToOne
	private Vertex destination;
	
	@Column(nullable = false)
	private double weight;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
		
	public Vertex getOrigin() {
		return origin;
	}
	public void setOrigin(Vertex origin) {
		this.origin = origin;
	}
	
	public Vertex getDestination() {
		return destination;
	}
	public void setDestination(Vertex destination) {
		this.destination = destination;
	}
	
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public Edge() {}
	
	public Edge(long id, Vertex origin, Vertex destination, double weight) {
		this.id = id;
		this.origin = origin;
		this.destination = destination;
		this.weight = weight;
	}
	
	@Override
	public String toString() {
		return "Edge [id=" + id + ", origin=" + origin + ", destination="
				+ destination + ", weight=" + weight + "]";
	}
	
}