package com.walmart.dto;

import java.util.ArrayList;
import java.util.List;

import com.walmart.entity.Vertex;

public class VertexDTO extends StatMsgDTO{
	
	List<Vertex> vertices = new ArrayList<Vertex>();

	public List<Vertex> getVertices() {
		return vertices;
	}
	public void setVertices(List<Vertex> vertices) {
		this.vertices = vertices;
	}
	
	public VertexDTO(){}
	
	public VertexDTO(List<Vertex> vertices) {
		this.vertices = vertices;
	}
	
	@Override
	public String toString() {
		return "VertexDTO [vertices=" + vertices + "]";
	}	

}
