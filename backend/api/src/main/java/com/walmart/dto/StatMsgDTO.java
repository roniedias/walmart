package com.walmart.dto;

import javax.ws.rs.core.Response.Status;

public class StatMsgDTO {
	
	private static Status status;
	private String message;
	
	public static Status getStatus() {
		return status;
	}
	public void setStatus(Status vStatus) {
		status = vStatus;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public StatMsgDTO() {}
	
	public StatMsgDTO(Status vStatus, String message) {
		status = vStatus;
		this.message = message;
	}
	
	@Override
	public String toString() {
		return "StatMsgDTO [status=" + status + ", message=" + message + "]";
	}
	

}
