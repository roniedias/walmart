package com.walmart.dto;

import java.util.List;

import com.walmart.entity.Edge;

public class EdgeDTO extends StatMsgDTO {
	
	private String vertexName;
	private List<Edge> edges;
	
	public String getVertexName() {
		return vertexName;
	}
	public void setVertexName(String vertexName) {
		this.vertexName = vertexName;
	}
	
	public List<Edge> getEdges() {
		return edges;
	}
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
	
	public EdgeDTO(){}
	
	public EdgeDTO(String vertexName, List<Edge> edges) {
		this.vertexName = vertexName;
		this.edges = edges;
	}
	
	@Override
	public String toString() {
		return "EdgeDTO [vertexName=" + vertexName + ", edges=" + edges + "]";
	}
	

}
