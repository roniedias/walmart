package com.walmart.service;

import com.walmart.dto.EdgeDTO;
import com.walmart.dto.StatMsgDTO;

public interface EdgeService {	
	EdgeDTO listAllEdges();
	StatMsgDTO createEdge(String data);
	StatMsgDTO removeEdge(String id);
}
