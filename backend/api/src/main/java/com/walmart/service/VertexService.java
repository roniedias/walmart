package com.walmart.service;

import com.walmart.dto.VertexDTO;
import com.walmart.dto.StatMsgDTO;

public interface VertexService {
	VertexDTO listAllVertices();
	StatMsgDTO createVertex(String data);
	StatMsgDTO removeVertex(String id);
}
