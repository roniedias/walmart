package com.walmart.service;

import com.walmart.dto.MapDTO;

public interface MapService {
	public MapDTO route(String origin, String destination, String autonomy, String fuelprice);
}
