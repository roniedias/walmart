package com.walmart.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;

import com.walmart.dto.EdgeDTO;
import com.walmart.dto.StatMsgDTO;
import com.walmart.entity.Edge;
import com.walmart.entity.Vertex;


@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class EdgeServiceImpl implements EdgeService {
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	VertexService vertexService;

	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public EdgeDTO listAllEdges() {
		
		EdgeDTO edgeDTO = new EdgeDTO();
		edgeDTO.setStatus(Status.OK);
		
		List<Edge> edges = em.createNamedQuery(Edge.QUERY_LIST_ALL_EDGES, Edge.class).getResultList();
		
		
		if(edges.size() == 0) {
			edgeDTO.setMessage("No record found");
		}
		else {
			edgeDTO.setMessage("Success");
			edgeDTO.setEdges(edges);
			
		}
		
		return edgeDTO;
	}
	
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public StatMsgDTO createEdge(String data) {
		
		/* Example of a valid json data:
		  {
		  	"origin": "A",
		  	"destination": "B",
		  	"distance": 10	
		  } 
		 */
		
		String origin = new String();
		String destination = new String();
		String distance = new String();
		
		
		Vertex ov;
		Vertex dv;
		Edge edge;
		
		
		
		StatMsgDTO smDTO = new StatMsgDTO();
		smDTO.setStatus(Status.BAD_REQUEST);	
		
		try {
			JSONObject jo = new JSONObject(data);
		    origin = jo.getString("origin");
		    destination = jo.getString("destination");
		    distance = jo.getString("distance");
		    
		    List<Vertex> originVertices = em.createNamedQuery(Vertex.QUERY_LIST_VERTEX_BY_NAME, Vertex.class).setParameter("name", origin.toUpperCase()).getResultList();
		    List<Vertex> destinationVertices = em.createNamedQuery(Vertex.QUERY_LIST_VERTEX_BY_NAME, Vertex.class).setParameter("name", destination.toUpperCase()).getResultList();
		    
		    if(originVertices.size() == 0) {
		    	smDTO.setMessage("Invalid ORIGIN: " + origin);
		    }
		    else if(destinationVertices.size() == 0) {
		    	smDTO.setMessage("Invalid DESTINATION: " + destination);
		    }
		    else if(origin.toUpperCase().equals(destination.toUpperCase())) {
		    	smDTO.setMessage("ORIGIN name cannot be equals to DESTINATION");
		    }
		    else {
		    	try {  
		    	    double distanceDb = Double.parseDouble(distance);
		    	    
		    	    if(distanceDb <= 0) {
		    	    	smDTO.setMessage("DISTANCE cannot be negative or equals to zero");
		    	    }
		    	    else {
		    	    	ov = new Vertex();
					    ov = originVertices.iterator().next();
					    
		    	    	dv = new Vertex();
					    dv = destinationVertices.iterator().next();
					    
					    List<Edge> edges = em.createNamedQuery(Edge.QUERY_LIST_ALL_EDGES, Edge.class).getResultList();
					    for(Edge e : edges) {
					    	if(e.getOrigin().getName().equals(origin.toUpperCase()) && e.getDestination().getName().equals(destination.toUpperCase())) {
					    		smDTO.setMessage("ORIGIN/DESTINATION already provided");
					    		return smDTO;
					    	}
					    }

		    	    	
					    edge = new Edge();
					    edge.setOrigin(ov);
					    edge.setDestination(dv);
					    edge.setWeight(distanceDb);
						em.persist(edge);
						em.flush();
						long id = edge.getId();
						smDTO.setStatus(Status.OK);
						smDTO.setMessage("Successfully persisted. ID: " + String.valueOf(id));
		    	    }
		    	  }  
		    	  catch(NumberFormatException nfe) {  
		    		  smDTO.setMessage("Invalid DISTANCE format: " + distance);
		    	  }  		    		
		    }
		        
		}
		catch (JSONException j) {
			if(origin == null || "".equals(origin)) {
				smDTO.setMessage("ORIGIN not informed");
			}
			else if(destination == null || "".equals(destination)) {
				smDTO.setMessage("DESTINATION not informed");
			}
			else if(distance == null || "".equals(distance)) {
				smDTO.setMessage("DISTANCE not informed");
			}

			else {
				smDTO.setMessage("Invalid data. JSON format expected");
			}
		}
		return smDTO;
	}

	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public StatMsgDTO removeEdge(String id) {
		
		StatMsgDTO smDTO = new StatMsgDTO();
		smDTO.setStatus(Status.BAD_REQUEST);
		
    	try {  
    		
    	    long longId = Long.parseLong(id);
    	    
    	    Edge e = em.find(Edge.class, longId);

    	    if(e == null) {
    			smDTO.setMessage("Invalid ID");
    	    }
    	    else {
    	    	em.remove(e);
    	    	smDTO.setStatus(Status.OK);
    	    	smDTO.setMessage("Successfully removed");
    	    }

    	}
    	catch(NumberFormatException nfe) {  
    		  smDTO.setMessage("Invalid ID format: " + id);
    	}  		    		
		  return smDTO;		
	}
	
}
