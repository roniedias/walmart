package com.walmart.service;

import java.util.LinkedList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response.Status;

import com.walmart.dto.MapDTO;
import com.walmart.entity.Edge;
import com.walmart.entity.Vertex;
import com.walmart.util.DijkstraAlgorithm;
import com.walmart.util.Graph;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MapServiceImpl implements MapService {
	
	//private static final Logger log = LoggerFactory.getLogger(MapServiceImpl.class);

	@PersistenceContext
	private EntityManager em;

	// Possible valid http call: http://localhost/walmart/api/rest/map/route?origin=A&destination=D&autonomy=10&fuelprice=6.25
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public MapDTO route(String origin, String destination, String autonomy, String fuelprice) {

		
		List<Vertex> vertices = em.createNamedQuery(Vertex.QUERY_LIST_ALL_VERTICES, Vertex.class).getResultList();
		List<Edge> edges = em.createNamedQuery(Edge.QUERY_LIST_ALL_EDGES, Edge.class).getResultList();
		
		boolean isValidOrigin = false;
		boolean isValidDestination = false;
		
		String ogn = null;
		String dstn = null;
		
		double distance = 0.0;
		String route = "Route: ";
		double total = 0.0;
		
		MapDTO mapDTO = new MapDTO();
		mapDTO.setStatus(Status.BAD_REQUEST);
		
		for(Vertex v : vertices) {
			if(v.getName().equals(origin.toUpperCase())) {
				isValidOrigin = true;
				ogn = origin.toUpperCase();
			}
			if(v.getName().equals(destination.toUpperCase())) {
				isValidDestination = true;
				dstn = destination.toUpperCase();
			}
		}
		
		if(!(isValidOrigin && isValidDestination)) {
			mapDTO.setMessage("Invalid ORIGIN and/or DESTINATION");
			return mapDTO;
		}
		
		try {
			Double.parseDouble(autonomy);	
		}
		catch(NumberFormatException nfe) {
			mapDTO.setMessage("Invalid AUTONOMY. Expected: Numeric format");
			return mapDTO;			
		}
		try {
			fuelprice.replaceAll(",", ".");
			Double.parseDouble(fuelprice);	
		}
		catch(NumberFormatException nfe) {
			mapDTO.setMessage("Invalid FUEL PRICE. Expected: Numeric format");
			return mapDTO;						
		}
			
	
		List<Vertex> ognLst = em.createNamedQuery(Vertex.QUERY_LIST_VERTEX_BY_NAME, Vertex.class).setParameter("name", ogn).getResultList();
		List<Vertex> dstnLst = em.createNamedQuery(Vertex.QUERY_LIST_VERTEX_BY_NAME, Vertex.class).setParameter("name", dstn).getResultList();
		
		Vertex ognVertex = ognLst.iterator().next();
		Vertex dstnVertex = dstnLst.iterator().next();
		
		try {
		    Graph graph = new Graph(vertices, edges);
		    DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
		    dijkstra.execute(ognVertex);
		    
		    distance = dijkstra.getShortestDistance(dstnVertex);
		    
		    mapDTO.setDistance(distance);
		    
		    LinkedList<Vertex> path = dijkstra.getPath(dstnVertex); 
		    for (Vertex vertex : path) {
		    	route += " " + vertex.getName();
		    }
		    
		    mapDTO.setRoute(route);
		    
		    total = distance / Double.valueOf(autonomy) * Double.valueOf(fuelprice);
		    
		    mapDTO.setTotal(total);
		    
		    mapDTO.setMessage("Success");
		    
		    mapDTO.setStatus(Status.OK);
	    
		}
		catch (NullPointerException n) {
			mapDTO.setMessage("Sorry! unable to calculate this route");
			return mapDTO;
		}

		return mapDTO;
				
	}
	
	

}
