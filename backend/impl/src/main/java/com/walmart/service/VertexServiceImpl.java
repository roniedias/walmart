package com.walmart.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Response.Status;

import org.json.JSONException;
import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.walmart.dto.StatMsgDTO;
import com.walmart.dto.VertexDTO;
import com.walmart.entity.Edge;
import com.walmart.entity.Vertex;


@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class VertexServiceImpl implements VertexService{
	
//	private static final Logger log = LoggerFactory.getLogger(VertexServiceImpl.class);
	
	@PersistenceContext
	private EntityManager em;
	
	@EJB
	EdgeService edgeService;

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public VertexDTO listAllVertices() {

		List<Vertex> vertices = em.createNamedQuery(Vertex.QUERY_LIST_ALL_VERTICES, Vertex.class).getResultList();
		
		VertexDTO vertexDTO = new VertexDTO();
		vertexDTO.setStatus(Status.OK);
		
		if(vertices.size() == 0) {
			vertexDTO.setMessage("No record found");
			
		}
		else {
			vertexDTO.setMessage("Success");
			vertexDTO.setVertices(vertices);
		}
		
		return vertexDTO;
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public StatMsgDTO createVertex(String data) {
		
		/* Example of a valid json data:
		  {
    		"name": "SP"
		  } 
		 */
		
		String name = new String();
		Vertex vertex = new Vertex();
		StatMsgDTO sDTO = new StatMsgDTO();
		sDTO.setStatus(Status.BAD_REQUEST);
		
		try {
			JSONObject jo = new JSONObject(data);
		    name = jo.getString("name");
		    
		    List<Vertex> vertices = em.createNamedQuery(Vertex.QUERY_LIST_VERTEX_BY_NAME, Vertex.class).setParameter("name", name.toUpperCase()).getResultList();
		    if(vertices.size() > 0) {
		    	sDTO.setMessage("Vertex NAME already exists: " + name);
		    }
		    else {		    
			    vertex.setName(name.toUpperCase());
				em.persist(vertex);
				em.flush();
				long id = vertex.getId();
				sDTO.setStatus(Status.OK);
				sDTO.setMessage("Successfully persisted. ID: " + String.valueOf(id));
		    }
		}
		catch (JSONException j) {
			if (name == null || "".equals(name)) {
				sDTO.setMessage("Vertex name not informed");				
			}
			else {
				sDTO.setMessage("Invalid data. JSON format expected");
			}
		}
		return sDTO;
	}
	
	
	
	@Override	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public StatMsgDTO removeVertex(String id) {
		
		StatMsgDTO smDTO = new StatMsgDTO();
		smDTO.setStatus(Status.BAD_REQUEST);

		long longId;
		
    	try {  
    		 longId = Long.parseLong(id);
    	}
    	catch(NumberFormatException nfe) {  
    		smDTO.setMessage("Invalid ID format: " + id);
    		return smDTO;
    	}

		List<Edge> edges = em.createNamedQuery(Edge.QUERY_LIST_ALL_EDGES, Edge.class).getResultList();
		for(Edge e: edges) {
			if(e.getOrigin().getId() == longId || e.getDestination().getId() == longId) {
				smDTO.setMessage("One or more Edges bound to this vertex. You must delete all dependent registers, before executing this operation");
				return smDTO;
			}
		}
		    
		Vertex v = em.find(Vertex.class, longId);
		
		if(v == null) {
    		smDTO.setMessage("Invalid ID");
    	}
		else { 	    	
			em.remove(v);
			smDTO.setMessage("Successfully removed");	    	    
		}

		smDTO.setStatus(Status.OK);
		return smDTO;		
	}

	

}
