package com.walmart.util;

import java.util.List;

import com.walmart.entity.Edge;
import com.walmart.entity.Vertex;

public class Graph {
	
	private List<Vertex> vertexes;
	private List<Edge> edges;

	public List<Vertex> getVertexes() {
		return vertexes;
	}
	public void setVertexes(List<Vertex> vertexes) {
		this.vertexes = vertexes;
	}

	public List<Edge> getEdges() {
		return edges;
	}
	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}
  
	public Graph() {}
  
	public Graph(List<Vertex> vertexes, List<Edge> edges) {
		this.vertexes = vertexes;
		this.edges = edges;
	}
  
}

  
